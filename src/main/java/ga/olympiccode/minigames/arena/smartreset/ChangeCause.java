package ga.olympiccode.minigames.arena.smartreset;

public enum ChangeCause {


    FADE, PHYSICS, BREAK, SPREAD, BURN, FROM_TO, ENTITY_CHANGE


}
