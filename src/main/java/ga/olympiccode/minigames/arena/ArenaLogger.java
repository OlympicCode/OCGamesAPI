package ga.olympiccode.minigames.arena;

import ga.olympiccode.minigames.OCGamesAPI;
import org.bukkit.Bukkit;

public class ArenaLogger {

    public static void debug(String msg) {
        if (OCGamesAPI.debug) {
            Bukkit.getConsoleSender().sendMessage("[" + System.currentTimeMillis() + " OCGAPI-DBG] " + msg);
        }
    }
}
