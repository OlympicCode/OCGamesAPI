package ga.olympiccode.minigames.arena;

public enum ArenaType {

    /**
     * Standard arena with lobby + spawn and lobby countdown; can have multiple spawns too
     */
    DEFAULT,

    /**
     * Players just join the game whenever they like, no lobby countdowns or arena/sign states
     */
    FFA,

    /**
     * Default arena + automatic regeneration
     */
    REGENERATION

}
