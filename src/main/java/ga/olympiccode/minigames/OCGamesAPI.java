package ga.olympiccode.minigames;



import ga.olympiccode.minigames.arena.Arena;
import ga.olympiccode.minigames.arena.ArenaListener;
import ga.olympiccode.minigames.arena.ArenaSetup;
import ga.olympiccode.minigames.arena.ArenaState;
import ga.olympiccode.minigames.bungee.MessageListener;
import ga.olympiccode.minigames.config.ArenasConfig;
import ga.olympiccode.minigames.config.DefaultConfig;
import ga.olympiccode.minigames.config.MessagesConfig;
import ga.olympiccode.minigames.config.StatsConfig;
import ga.olympiccode.minigames.config.ClassesConfig;
import ga.olympiccode.minigames.configs.PartyMessagesConfig;
import ga.olympiccode.minigames.configs.StatsGlobalConfig;
import ga.olympiccode.minigames.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;

public class OCGamesAPI extends JavaPlugin {

    static OCGamesAPI instance = null;
    public static boolean debug = false;

    public HashMap<String, Party> global_party = new HashMap<String, Party>();
    public HashMap<String, ArrayList<Party>> global_party_invites = new HashMap<String, ArrayList<Party>>();

    public static HashMap<JavaPlugin, PluginInstance> pinstances = new HashMap<JavaPlugin, PluginInstance>();

    public PartyMessagesConfig partymessages;
    public StatsGlobalConfig statsglobal;

    public String version = "";

    public boolean below1_9 = false;



    @Override
    public void onEnable() {
        instance = this;
        this.version = Bukkit.getServer().getClass().getPackage().getName().substring(Bukkit.getServer().getClass().getPackage().getName().lastIndexOf(".") + 1);
        this.below1_9 = version.startsWith("v1_8");
        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Loaded OCGamesAPI with " + version + " support.");

        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new MessageListener());

        getConfig().addDefault("config.party_command_enabled", true);
        getConfig().addDefault("config.debug", false);

        getConfig().options().copyDefaults(true);
        saveConfig();

        partymessages = new PartyMessagesConfig(this);
        statsglobal = new StatsGlobalConfig(this, false);

        this.debug = getConfig().getBoolean("config.debug");
    }

    @Override
    public void onDisable() {
        for (PluginInstance pli : this.pinstances.values()) {
            // Reset arenas
            for (Arena a : pli.getArenas()) {
                if (a != null) {
                    if (a.isSuccessfullyInit()) {
                        if (a.getArenaState() != ArenaState.JOIN) {
                            a.getSmartReset().saveSmartBlocksToFile();
                        }
                        ArrayList<String> temp = new ArrayList<String>(a.getAllPlayers());
                        for (String p_ : temp) {
                            a.leavePlayer(p_, true);
                        }
                        try {
                            a.getSmartReset().resetRaw();
                        } catch (Exception e) {
                            System.out.println("Failed resetting arena at onDisable. " + e.getMessage());
                        }
                    } else {
                        System.out.println(a.getName() + " not initialized thus not reset at onDisable.");
                    }
                }
            }

            // Save important configs
            pli.getArenasConfig().saveConfig();
            pli.getPlugin().saveConfig();
            pli.getMessagesConfig().saveConfig();
            pli.getClassesConfig().saveConfig();
        }
    }

    /**
     * Sets up the API allowing to override all configs
     *
     * @param plugin_
     * @param arenaclass
     * @param arenasconfig
     * @param messagesconfig
     * @param classesconfig
     * @param statsconfig
     * @return
     */
    public static OCGamesAPI setupAPI(JavaPlugin plugin_, String minigame, Class<?> arenaclass, ArenasConfig arenasconfig, MessagesConfig messagesconfig, ClassesConfig classesconfig, StatsConfig statsconfig, DefaultConfig defaultconfig, boolean customlistener) {
        pinstances.put(plugin_, new PluginInstance(plugin_, arenasconfig, messagesconfig, classesconfig, statsconfig, new ArrayList<Arena>()));
        if (!customlistener) {
            ArenaListener al = new ArenaListener(plugin_, pinstances.get(plugin_), minigame);
            pinstances.get(plugin_).setArenaListener(al);
            Bukkit.getPluginManager().registerEvents(al, plugin_);
        }
        Classes.loadClasses(plugin_);
        pinstances.get(plugin_).getShopHandler().loadShopItems();
        return instance;
    }

    public static void registerArenaListenerLater(JavaPlugin plugin_, ArenaListener arenalistener) {
        Bukkit.getPluginManager().registerEvents(arenalistener, plugin_);
    }

    public static void registerArenaSetup(JavaPlugin plugin_, ArenaSetup arenasetup) {
        pinstances.get(plugin_).arenaSetup = arenasetup;
    }

    public static void registerScoreboard(JavaPlugin plugin_, ArenaScoreboard board) {
        pinstances.get(plugin_).scoreboardManager = board;
    }

    /**
     * Sets up the API, stuff won't work without that
     *
     * @param plugin_
     * @return
     */
    // Allow loading of arenas with own extended arena class into
    // PluginInstance:
    // after this setup, get the PluginInstance, load the arenas by yourself
    // and add the loaded arenas w/ custom arena class into the PluginInstance
    public static OCGamesAPI setupAPI(JavaPlugin plugin_, String minigame, Class<?> arenaclass) {
        setupRaw(plugin_, minigame);
        return instance;
    }

    /**
     * Sets up the API, stuff won't work without that
     *
     * @param plugin_
     * @return
     */
    public static OCGamesAPI setupAPI(JavaPlugin plugin_, String minigame) {
        PluginInstance pli = setupRaw(plugin_, minigame);
        pli.addLoadedArenas(Utils.loadArenas(plugin_, pli.getArenasConfig()));
        return instance;
    }

    public static PluginInstance setupRaw(JavaPlugin plugin_, String minigame) {
        ArenasConfig arenasconfig = new ArenasConfig(plugin_);
        MessagesConfig messagesconfig = new MessagesConfig(plugin_);
        ClassesConfig classesconfig = new ClassesConfig(plugin_, false);
        StatsConfig statsconfig = new StatsConfig(plugin_, false);
        DefaultConfig.init(plugin_, false);
        PluginInstance pli = new PluginInstance(plugin_, arenasconfig, messagesconfig, classesconfig, statsconfig);
        pinstances.put(plugin_, pli);
        ArenaListener al = new ArenaListener(plugin_, pinstances.get(plugin_), minigame);
        pinstances.get(plugin_).setArenaListener(al);
        Bukkit.getPluginManager().registerEvents(al, plugin_);
        Classes.loadClasses(plugin_);
        pli.getShopHandler().loadShopItems();
        return pli;
    }

    public static OCGamesAPI getAPI() {
        return instance;
    }

    public static CommandHandler getCommandHandler() {
        return new CommandHandler();
    }

    public PluginInstance getPluginInstance(JavaPlugin plugin) {
        return pinstances.get(plugin);
    }

}
