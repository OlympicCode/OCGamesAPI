package ga.olympiccode.minigames.bungee;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import ga.olympiccode.minigames.OCGamesAPI;
import ga.olympiccode.minigames.arena.Arena;
import ga.olympiccode.minigames.arena.ArenaState;
import ga.olympiccode.minigames.utils.BungeeUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class MessageListener implements PluginMessageListener {

    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();
        System.out.println(subchannel);
        if (subchannel.equals("OCGamesApiBack")) {
            short len = in.readShort();
            byte[] msgbytes = new byte[len];
            in.readFully(msgbytes);

            DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(msgbytes));
            try {
                final String playerData = msgin.readUTF();
                final String plugin_ = playerData.split(":")[0];
                final String arena = playerData.split(":")[1];
                final String playername = playerData.split(":")[2];
                System.out.println(plugin_ + " -> " + arena);
                JavaPlugin plugin = null;
                for (JavaPlugin pl : OCGamesAPI.getAPI().pinstances.keySet()) {
                    if (pl.getName().contains(plugin_)) {
                        plugin = pl;
                        break;
                    }
                }
                if (plugin != null) {
                    final Arena a = OCGamesAPI.getAPI().pinstances.get(plugin).getArenaByName(arena);
                    if (a != null) {
                        if (a.getArenaState() != ArenaState.INGAME && a.getArenaState() != ArenaState.RESTARTING && !a.containsPlayer(playername)) {
                            Bukkit.getScheduler().runTaskLater(OCGamesAPI.getAPI(), new Runnable() {
                                public void run() {
                                    if (!a.containsPlayer(playername)) {
                                        a.joinPlayerLobby(playername);
                                    }
                                }
                            }, 20L);
                        }
                    } else {
                        System.out.println("Arena " + arena + " couldn't be found, please fix your setup.");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (subchannel.equals("OCGamesApiRequest")) { // Lobby requests sign data
            short len = in.readShort();
            byte[] msgbytes = new byte[len];
            in.readFully(msgbytes);

            DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(msgbytes));
            try {
                final String requestData = msgin.readUTF();
                final String plugin_ = requestData.split(":")[0];
                final String arena = requestData.split(":")[1];
                System.out.println(plugin_ + " -> " + arena);
                for (JavaPlugin pl : OCGamesAPI.getAPI().pinstances.keySet()) {
                    if (pl.getName().contains(plugin_)) {
                        Arena a = OCGamesAPI.getAPI().pinstances.get(pl).getArenaByName(arena);
                        if (a != null) {
                            BungeeUtils.sendSignUpdateRequest(pl, pl.getName(), a);
                        } else {
                            System.out.println("Arena " + arena + " couldn't be found, please fix your setup.");
                        }
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
