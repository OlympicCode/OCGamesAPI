package ga.olympiccode.minigames;

import ga.olympiccode.minigames.arena.Arena;
import ga.olympiccode.minigames.utils.ParticleEffectNew;
import ga.olympiccode.minigames.utils.Validator;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Effects {

    public static int getClientProtocolVersion(Player p) {
        int ret = 0;
        try {
            if (!OCGamesAPI.getAPI().version.equalsIgnoreCase("v1_8_r1")) {
                Method getHandle = Class.forName("org.bukkit.craftbukkit." + OCGamesAPI.getAPI().version + ".entity.CraftPlayer").getMethod("getHandle");
                Field playerConnection = Class.forName("net.minecraft.server." + OCGamesAPI.getAPI().version + ".EntityPlayer").getField("playerConnection");
                playerConnection.setAccessible(true);
                Object playerConInstance = playerConnection.get(getHandle.invoke(p));
                Field networkManager = playerConInstance.getClass().getField("networkManager");
                networkManager.setAccessible(true);
                Object networkManagerInstance = networkManager.get(playerConInstance);
                Method getVersion = networkManagerInstance.getClass().getMethod("getVersion");
                Object version = getVersion.invoke(networkManagerInstance);
                ret = (Integer) version;
            }
        } catch (Exception e) {
            if (OCGamesAPI.debug) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * Shows the particles of a redstone block breaking
     *
     * @param p
     */
    public static void playBloodEffect(Player p) {
        p.getWorld().playEffect(p.getLocation().add(0D, 1D, 0D), Effect.STEP_SOUND, 152);
    }

    public static void playEffect(Arena a, Location l, String effectname) {
        for (String p_ : a.getAllPlayers()) {
            if (Validator.isPlayerOnline(p_)) {
                Player p = Bukkit.getPlayer(p_);
                ParticleEffectNew eff = ParticleEffectNew.valueOf(effectname);
                eff.setId(152);
                eff.animateReflected(p, l, 1F, 3);
            }
        }
    }
}
