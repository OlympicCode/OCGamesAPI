package ga.olympiccode.minigames.configs;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StatsGlobalConfig {

    private FileConfiguration statsConfig = null;
    private File statsFile = null;
    private JavaPlugin plugin = null;

    public StatsGlobalConfig(JavaPlugin plugin, boolean custom){
        this.plugin = plugin;
        if(!custom){
            this.getConfig().options().header("Used for saving user statistics. Example user stats:");
            this.getConfig().addDefault("players.a34d9cbd-1c17-4be5-b7b3-eb4c88dc07ee.wins", 1);
            this.getConfig().addDefault("players.a34d9cbd-1c17-4be5-b7b3-eb4c88dc07ee.loses", 1);
            this.getConfig().addDefault("players.a34d9cbd-1c17-4be5-b7b3-eb4c88dc07ee.points", 10);
            this.getConfig().addDefault("players.a34d9cbd-1c17-4be5-b7b3-eb4c88dc07ee.playername", "Checkium");
        }
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }

    public FileConfiguration getConfig() {
        if (statsConfig == null) {
            reloadConfig();
        }
        return statsConfig;
    }

    public void saveConfig() {
        if (statsConfig == null || statsFile == null) {
            return;
        }
        try {
            getConfig().save(statsFile);
        } catch (IOException ex) {

        }
    }

    public void reloadConfig() {
        if (statsFile == null) {
            statsFile = new File(plugin.getDataFolder(), "global_stats.yml");
        }
        statsConfig = YamlConfiguration.loadConfiguration(statsFile);

        InputStream defConfigStream = plugin.getResource("global_stats.yml");
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream));
            statsConfig.setDefaults(defConfig);
        }
    }
}
