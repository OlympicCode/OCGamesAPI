package ga.olympiccode.minigames.utils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import ga.olympiccode.minigames.OCGamesAPI;
import ga.olympiccode.minigames.PluginInstance;
import ga.olympiccode.minigames.arena.Arena;
import ga.olympiccode.minigames.bungee.BungeeSocket;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class BungeeUtils {

    public static void connectToServer(JavaPlugin plugin, String player, String server) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF("Connect");
            out.writeUTF(server);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bukkit.getPlayer(player).sendPluginMessage(plugin, "BungeeCord", stream.toByteArray());
    }

    public static void sendSignUpdateRequest(JavaPlugin plugin, String minigame, Arena arena) {
        PluginInstance pli = OCGamesAPI.getAPI().getPluginInstance(plugin);
        BungeeSocket.sendSignUpdate(pli, arena);
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        try {
            out.writeUTF("Forward");
            out.writeUTF("ALL");
            out.writeUTF("OCGamesAPI");

            ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
            DataOutputStream msgout = new DataOutputStream(msgbytes);
            msgout.writeUTF(minigame + ":" + arena.getInternalName() + ":" + arena.getArenaState().toString() + ":" + Integer.toString(arena.getAllPlayers().size()) + ":" + Integer.toString(arena.getMaxPlayers()));

            out.writeShort(msgbytes.toByteArray().length);
            out.write(msgbytes.toByteArray());

            Bukkit.getServer().sendPluginMessage(OCGamesAPI.getAPI(), "BungeeCord", out.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
